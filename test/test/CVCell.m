//
//  CVCell.m
//  test
//
//  Created by Abhimanyu Bhatnagar on 08/05/16.
//  Copyright © 2016 Abhimanyu Bhatnagar. All rights reserved.
//

#import "CVCell.h"

@implementation CVCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        NSArray *arrViews = [[NSBundle mainBundle] loadNibNamed:@"CVCell" owner:self options:nil];
        
        if ([arrViews count] < 1) {
            return nil;
        }
        
        if (![[arrViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        
        self = [arrViews objectAtIndex:0];
        
    }
    
    return self;
    
}


@end
