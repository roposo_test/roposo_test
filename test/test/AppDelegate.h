//
//  AppDelegate.h
//  test
//
//  Created by Abhimanyu Bhatnagar on 08/05/16.
//  Copyright © 2016 Abhimanyu Bhatnagar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

