//
//  DetailViewController.m
//  test
//
//  Created by Abhimanyu Bhatnagar on 08/05/16.
//  Copyright © 2016 Abhimanyu Bhatnagar. All rights reserved.
//

#import "DetailViewController.h"
#import "UIImageEffects.h"
#import "UIImageView+WebCache.h"
@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"Details";
    self.view.backgroundColor = [UIColor colorWithRed:102.0/255.0 green:255.0/255.0 blue:204.0/255.0 alpha:1];

    [self getValuesFromPlist];
    [self designUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Set Display Info
-(void)setDisplayInformation
{

}

#pragma mark - Designing the view
-(void)designUI
{
    //Checking if it is a story or user
    BOOL _bUser=YES;
    if([[_marrData objectAtIndex:_indexSelected] valueForKey:@"type"] ==nil)       //User
    {
        _bUser=YES;
    }
    else
    {
        _bUser=NO;
    }

    NSString *strImageURL=[[NSString alloc]init];
    if(_bUser==YES)
        strImageURL=[[NSString alloc]initWithFormat:@"%@",[[_marrData objectAtIndex:_indexSelected] valueForKey:@"image"]];
    else
        strImageURL=[[NSString alloc]initWithFormat:@"%@",[[_marrData objectAtIndex:_indexSelected] valueForKey:@"si"]];
    NSURL *URLImage=[[NSURL alloc]initWithString:strImageURL];
    
    //Imageview to display the image of the user or the story image
    UIImageView *imgViewPhoto = [[UIImageView alloc]init];
    imgViewPhoto.frame = CGRectMake(self.view.frame.origin.x+20, self.view.frame.origin.y+70, 100, 100);
    [imgViewPhoto sd_setImageWithURL:URLImage placeholderImage:[UIImage imageNamed:@"default_image"]];
    [self.view addSubview:imgViewPhoto];
    
    //Adding blur image for design
    UIImageView *imgViewBlur = [[UIImageView alloc]init];
    imgViewBlur.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+60, self.view.frame.size.width, 150);
    [self.view addSubview:imgViewBlur];
    [imgViewBlur setImage:[UIImageEffects imageByApplyingDarkEffectToImage:imgViewPhoto.image]];
    
    [self.view bringSubviewToFront:imgViewPhoto];
    
    //Adding follow/handle
    UILabel *lblHandle = [[UILabel alloc]init];
    lblHandle.frame = CGRectMake(imgViewPhoto.frame.origin.x, imgViewPhoto.frame.origin.y+imgViewPhoto.frame.size.height+10, self.view.frame.size.width/2, 20);
    if(_bUser==YES)
        [lblHandle setText:[[_marrData objectAtIndex:_indexSelected] valueForKey:@"handle"]];
    else
        [lblHandle setText:[[_marrData objectAtIndex:_indexSelected] valueForKey:@"handle"]];
    lblHandle.numberOfLines=0;
    lblHandle.textColor=[UIColor whiteColor];
    lblHandle.backgroundColor = [UIColor clearColor];
    [lblHandle sizeToFit];
    [self.view addSubview:lblHandle];

    //Adding title/username
    UILabel *lblTitle = [[UILabel alloc]init];
    lblTitle.frame = CGRectMake(self.view.frame.size.width/2, imgViewPhoto.frame.origin.y, self.view.frame.size.width/2, 30);
    lblTitle.textColor = [UIColor whiteColor];
    if(_bUser==YES)
        lblTitle.text = [NSString stringWithFormat:@"%@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"username"]];
    else
        lblTitle.text = [NSString stringWithFormat:@"%@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"title"]];
    lblTitle.textAlignment = NSTextAlignmentJustified;
    
    [self.view addSubview:lblTitle];
    
    //Adding scrollview to display all the information
    UIScrollView *scrollViewInfo = [[UIScrollView alloc]init];
    scrollViewInfo.frame = CGRectMake(0, imgViewBlur.frame.size.height+imgViewBlur.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-imgViewBlur.frame.size.height-imgViewBlur.frame.origin.y-50);
    scrollViewInfo.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:scrollViewInfo];
    
    //Adding about or description
    UILabel *lblAbout = [[UILabel alloc]init];
    lblAbout.frame = CGRectMake(10, 20, scrollViewInfo.frame.size.width-20, 50);
    if(_bUser==YES)
        lblAbout.text = [[_marrData objectAtIndex:_indexSelected] valueForKey:@"about"];
    else
        lblAbout.text = [[_marrData objectAtIndex:_indexSelected] valueForKey:@"description"];
    if(!(lblAbout.text.length>0))
    {
        [lblAbout setText:[NSString stringWithFormat:@"%@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"title"]]];
        //        lblAbout.text = lblTitle.text;
    }
    lblAbout.numberOfLines=0;
    lblAbout.textAlignment=NSTextAlignmentCenter;
    [lblAbout sizeToFit];

    [scrollViewInfo addSubview:lblAbout];
    
    //Adding URL
    UIButton *btnUrl = [UIButton buttonWithType:UIButtonTypeCustom];
    btnUrl.frame = CGRectMake(0,scrollViewInfo.frame.origin.y+scrollViewInfo.frame.size.height, self.view.frame.size.width, 50);
    [btnUrl setTitle:@"Visit Website" forState:UIControlStateNormal];
    [btnUrl setTitle:[NSString stringWithFormat:@"%@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"url"]] forState:UIControlStateReserved];
    btnUrl.backgroundColor = [UIColor blueColor];
    [btnUrl addTarget:self action:@selector(visitWebSite:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnUrl];
    
    
    
    
    
    //Adding followers and following
    UILabel *lblFollowers = [[UILabel alloc]init];
    lblFollowers.frame = CGRectMake(lblTitle.frame.origin.x, lblTitle.frame.origin.y+lblTitle.frame.size.height, lblTitle.frame.size.width, lblTitle.frame.size.height);
    lblFollowers.textAlignment=NSTextAlignmentJustified;
    if(_bUser==YES)
        lblFollowers.text = [NSString stringWithFormat:@"Followers: %@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"followers"]];
    else
        lblFollowers.text = [NSString stringWithFormat:@"Comments: %@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"comment_count"]];
    lblFollowers.textColor = [UIColor whiteColor];
    [self.view addSubview:lblFollowers];
    
    UILabel *lblFollowing = [[UILabel alloc]init];
    lblFollowing.frame = CGRectMake(lblFollowers.frame.origin.x, lblFollowers.frame.origin.y+lblFollowers.frame.size.height, lblFollowers.frame.size.width, lblFollowers.frame.size.height);
    lblFollowing.textAlignment=NSTextAlignmentJustified;
    if(_bUser==YES)
        lblFollowing.text = [NSString stringWithFormat:@"Following: %@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"following"]];
    else
        lblFollowing.text = [NSString stringWithFormat:@"Likes: %@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"likes_count"]];
    lblFollowing.textColor = [UIColor whiteColor];
    [self.view addSubview:lblFollowing];
    
    //Adding follow button
    UIButton *btnFollow = [UIButton buttonWithType:UIButtonTypeCustom];
    btnFollow.frame = CGRectMake(self.view.frame.size.width*0.70, lblHandle.frame.origin.y, self.view.frame.size.width*0.25, 20);
    
    if(_bUser==YES)
    {
        if([arrAllKeys containsObject:[[_marrData objectAtIndex:_indexSelected] valueForKey:@"id"]])
            [btnFollow setTitle:@"Following" forState:UIControlStateNormal];
        else
            [btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
        [btnFollow setTitle:[NSString stringWithFormat:@"%@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"id"]] forState:UIControlStateReserved];
    }
    else
    {
        if([arrAllKeys containsObject:[[_marrData objectAtIndex:_indexSelected] valueForKey:@"db"]])
            [btnFollow setTitle:@"Following" forState:UIControlStateNormal];
        else
            [btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
        [btnFollow setTitle:[NSString stringWithFormat:@"%@", [[_marrData objectAtIndex:_indexSelected] valueForKey:@"db"]] forState:UIControlStateReserved];
    }
    [btnFollow addTarget:self action:@selector(btnFollowClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnFollow setUserInteractionEnabled:YES];
    btnFollow.backgroundColor = [UIColor blueColor];
    btnFollow.alpha=0.7;
    btnFollow.titleLabel.font = [UIFont systemFontOfSize:12];
    btnFollow.layer.cornerRadius=10.0;
    btnFollow.clipsToBounds=YES;
    [self.view addSubview:btnFollow];
}

#pragma mark - Follow button clicked
-(void)btnFollowClicked:(UIButton *)sender
{
    NSMutableDictionary *mdic = [[NSMutableDictionary alloc]initWithContentsOfFile:strPathForPlist];
    NSLog(@"%@", [sender titleForState:UIControlStateReserved]);
    if(![[sender titleForState:UIControlStateNormal] caseInsensitiveCompare:@"follow"])         //Not following
    {
        [sender setTitle:@"Following" forState:UIControlStateNormal];
        [mdic setObject:[sender titleForState:UIControlStateReserved] forKey:[sender titleForState:UIControlStateReserved]];
        [mdic writeToFile:strPathForPlist atomically:YES];
    }
    else                        //Already following
    {
        [sender setTitle:@"Follow" forState:UIControlStateNormal];
        [mdic removeObjectForKey:[sender titleForState:UIControlStateReserved]];
        [mdic writeToFile:strPathForPlist atomically:YES];
    }
}

#pragma mark - Visit website
-(void)visitWebSite:(UIButton *)sender
{
    NSString* url = [NSString stringWithFormat:@"%@", [sender titleForState:UIControlStateReserved]];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
}

#pragma mark - Get Values from Plist
-(void)getValuesFromPlist
{
    documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    strPathForPlist = [documentsDirectory stringByAppendingPathComponent:@"follow.plist"];
    NSMutableDictionary *savedValue = [[NSMutableDictionary alloc] initWithContentsOfFile: strPathForPlist];
    arrAllKeys = [[NSArray alloc]init];
    arrAllKeys = [savedValue allKeys];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
