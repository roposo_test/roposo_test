//
//  CVCell.h
//  test
//
//  Created by Abhimanyu Bhatnagar on 08/05/16.
//  Copyright © 2016 Abhimanyu Bhatnagar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CVCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgViewPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblHandle;
@property (weak, nonatomic) IBOutlet UILabel *lblAbout;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewStoryPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnFollow;


@end
