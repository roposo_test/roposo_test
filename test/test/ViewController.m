//
//  ViewController.m
//  test
//
//  Created by Abhimanyu Bhatnagar on 08/05/16.
//  Copyright © 2016 Abhimanyu Bhatnagar. All rights reserved.
//

#import "ViewController.h"
#import "CVCell.h"
#import "UIImageView+WebCache.h"
#import "DetailViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.title = @"Story Feed";
    self.view.backgroundColor = [UIColor whiteColor];
    [self addPlist];
    [self initialize];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getValuesFromPlist];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Data storage
-(void)addPlist
{
    documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    strPathForPlist = [documentsDirectory stringByAppendingPathComponent:@"follow.plist"];
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"FirstRun"]){
        //        documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        //        strPathForPlist = [documentsDirectory stringByAppendingPathComponent:@"follow.plist"];
        
        [@{} writeToFile: strPathForPlist atomically: YES];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"FirstRun"];
    }
}
#pragma mark - Initialization
-(void)initialize
{
    marrDataToDisplay = [[NSMutableArray alloc]init];
    
    //Parsing data from json
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"iOS_Android_Data" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    marrDataToDisplay = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    //Making collectionview
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    [layout setMinimumLineSpacing:25];
    [layout setMinimumInteritemSpacing:5];
    collectionViewStoryCards = [[UICollectionView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x+10, self.view.frame.origin.y+10, self.view.frame.size.width-20, self.view.frame.size.height-20) collectionViewLayout:layout];
    [collectionViewStoryCards setDataSource:self];
    [collectionViewStoryCards setDelegate:self];
    
    //Registering collectionview with the cell identifier
    [collectionViewStoryCards registerClass:[CVCell class] forCellWithReuseIdentifier:@"cvCell"];
    [collectionViewStoryCards setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:collectionViewStoryCards];
    
    [self getValuesFromPlist];
}

#pragma mark - Collectionview delegates and datasource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cvCell";
    
    CVCell *cell = (CVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if([[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"type"] ==nil)       //User
    {
        NSString *strImageURL=[[NSString alloc]initWithFormat:@"%@",[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"image"]];
        NSURL *URLImage=[[NSURL alloc]initWithString:strImageURL];
        
        [cell.imgViewPhoto sd_setImageWithURL:URLImage placeholderImage:[UIImage imageNamed:@"default_image"]];
        
        [cell.lblAbout setText:[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"about"]];
        [cell.lblTitle setText:[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"username"]];
        [cell.lblHandle setText:[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"handle"]];
        [cell.btnFollow setTitle:[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"id"] forState:UIControlStateReserved];
        if([arrAllKeys containsObject:[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"id"]])
            [cell.btnFollow setTitle:@"Following" forState:UIControlStateNormal];
        else
            [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
        cell.lblAbout.hidden=NO;
        cell.imgViewStoryPhoto.hidden=YES;
        
    }
    else                //Story
    {
        NSString *strImageURL=[[NSString alloc]initWithFormat:@"%@",[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"si"]];
        NSURL *URLImage=[[NSURL alloc]initWithString:strImageURL];
        [cell.imgViewPhoto sd_setImageWithURL:URLImage placeholderImage:[UIImage imageNamed:@"default_image"]];
        [cell.lblAbout setText:[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"description"]];
        [cell.lblTitle setText:[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"title"]];
        [cell.btnFollow setTitle:[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"db"] forState:UIControlStateReserved];
        if([arrAllKeys containsObject:[[marrDataToDisplay objectAtIndex:indexPath.row] valueForKey:@"db"]])
            [cell.btnFollow setTitle:@"Following" forState:UIControlStateNormal];
        
        else
            [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
        [cell.lblHandle setText:@""];
        
        //Showing image if description is empty
        if([cell.lblAbout.text isEqualToString:@""])
        {
            [cell.imgViewStoryPhoto sd_setImageWithURL:URLImage placeholderImage:[UIImage imageNamed:@"default_image"]];
            [cell.imgViewStoryPhoto setContentMode:UIViewContentModeScaleAspectFit];
            cell.lblAbout.hidden=YES;
            cell.imgViewStoryPhoto.hidden=NO;
        }
        else
        {
            cell.lblAbout.hidden=NO;
            cell.imgViewStoryPhoto.hidden=YES;
        }
    }
    cell.btnFollow.tag=(int)indexPath.row+100;
    cell.btnFollow.backgroundColor = [UIColor whiteColor];
    cell.btnFollow.layer.cornerRadius=10.0;
    cell.btnFollow.clipsToBounds=YES;
    cell.btnFollow.alpha=0.7;
    [cell.btnFollow addTarget:self action:@selector(btnFollowClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.lblAbout.backgroundColor=[UIColor clearColor];
    cell.lblHandle.backgroundColor=[UIColor clearColor];
    cell.lblTitle.backgroundColor = [UIColor clearColor];
    cell.imgViewPhoto.layer.cornerRadius=cell.imgViewPhoto.frame.size.width/2;
    cell.imgViewPhoto.clipsToBounds=YES;
    [cell.imgViewPhoto setBackgroundColor:[UIColor clearColor]];
    
    // Return the cell
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return marrDataToDisplay.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width, collectionView.frame.size.height/2);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    DetailViewController *objDetailVC = [[DetailViewController alloc]init];
    objDetailVC.indexSelected=(NSInteger)indexPath.row;
    objDetailVC.marrData=marrDataToDisplay;
    [self.navigationController pushViewController:objDetailVC animated:YES];
}

#pragma mark - Button actions
-(void)btnFollowClicked:(UIButton *)sender
{
    NSMutableDictionary *mdic = [[NSMutableDictionary alloc]initWithContentsOfFile:strPathForPlist];
    if(![[sender titleForState:UIControlStateNormal] caseInsensitiveCompare:@"follow"])         //Not following
    {
        [mdic setObject:[sender titleForState:UIControlStateReserved] forKey:[sender titleForState:UIControlStateReserved]];
        [mdic writeToFile:strPathForPlist atomically:YES];
    }
    else                        //Already following
    {
        [mdic removeObjectForKey:[sender titleForState:UIControlStateReserved]];
        [mdic writeToFile:strPathForPlist atomically:YES];
    }
    
    [self getValuesFromPlist];
}

#pragma mark - Get Values from Plist
-(void)getValuesFromPlist
{
    NSMutableDictionary *savedValue = [[NSMutableDictionary alloc] initWithContentsOfFile: strPathForPlist];
    arrAllKeys = [[NSArray alloc]init];
    arrAllKeys = [savedValue allKeys];
    [collectionViewStoryCards reloadData];
}
@end
