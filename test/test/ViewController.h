//
//  ViewController.h
//  test
//
//  Created by Abhimanyu Bhatnagar on 08/05/16.
//  Copyright © 2016 Abhimanyu Bhatnagar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    UICollectionView *collectionViewStoryCards;
    NSMutableArray *marrDataToDisplay;
    NSString *documentsDirectory;
    NSString *strPathForPlist;
    NSArray *arrAllKeys;
}

@end

