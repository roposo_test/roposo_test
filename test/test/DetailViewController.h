//
//  DetailViewController.h
//  test
//
//  Created by Abhimanyu Bhatnagar on 08/05/16.
//  Copyright © 2016 Abhimanyu Bhatnagar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController
{
    NSString *documentsDirectory;
    NSString *strPathForPlist;
    NSArray *arrAllKeys;
}
@property (nonatomic) NSInteger indexSelected;
@property (nonatomic) NSMutableArray *marrData;
@end
